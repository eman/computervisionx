from typing import Tuple, Union
import cv2
import numpy as np
import mediapipe as mp
import face_recognition

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_holistic = mp.solutions.holistic
mp_face_mesh = mp.solutions.face_mesh

import pose, face

ARM_CONFIG_PATH = "arms.json"
#FACE_CONFIG_PATH = "face.pt"
FACE_CONFIG_PATH = "set/me.jpg"

class Camera():

    _capture : cv2.VideoCapture

    def __init__(self, source : Union[int, cv2.VideoCapture] = 0, force : bool = True):
        if isinstance(source, int):
            self._capture = cv2.VideoCapture(source)
        elif isinstance(source, cv2.VideoCapture):
            self._capture = source
        else:
            raise ValueError("Invalid parameter type")

        if force and not self.isOpened():
            raise RuntimeError()

    def __enter__(self) -> "Camera":
        return self

    def __exit__(self, *args):
        self.release()

    def isOpened(self) -> bool:
        return self._capture.isOpened()

    def read(self, invert=True) -> Tuple[bool, np.ndarray]:
        ret, img = self._capture.read()
        if ret and invert:
            img = cv2.flip(img, 1)
        return ret, img

    def release(self):
        self._capture.release()

class ProcessImage():

    _holistic : mp_holistic.Holistic
    # Holistic : pose, face, left_hand, right_hand + segmentation mask
    #https://google.github.io/mediapipe/solutions/pose.html
    #https://google.github.io/mediapipe/solutions/hands.html
    #https://google.github.io/mediapipe/solutions/face_mesh.html
    
    def __init__(self):        
        pass

    def __enter__(self) -> "ProcessImage":
        self._holistic = mp_holistic.Holistic(enable_segmentation=True)
        return self

    def __exit__(self, *args):
        self._holistic.close()

    def draw_results(self, annotated_image, results):

        mp_drawing.draw_landmarks(
            annotated_image,
            results.face_landmarks,
            mp_holistic.FACEMESH_CONTOURS,
            landmark_drawing_spec=None, 
            connection_drawing_spec=mp_drawing_styles
            .get_default_face_mesh_contours_style())
        
        mp_drawing.draw_landmarks(
            annotated_image,
            results.face_landmarks,
            mp_holistic.FACEMESH_TESSELATION, 
            landmark_drawing_spec=None, 
            connection_drawing_spec=mp_drawing_styles
            .get_default_face_mesh_tesselation_style())
        
        mp_drawing.draw_landmarks(
            annotated_image,
            results.pose_landmarks,
            mp_holistic.POSE_CONNECTIONS,
            landmark_drawing_spec=mp_drawing_styles
            .get_default_pose_landmarks_style())
        
        mp_drawing.draw_landmarks(
            annotated_image,
            results.left_hand_landmarks,
            mp_holistic.HAND_CONNECTIONS,
            landmark_drawing_spec=mp_drawing_styles
            .get_default_hand_landmarks_style(),
            connection_drawing_spec=mp_drawing_styles
            .get_default_hand_connections_style())
        
        mp_drawing.draw_landmarks(
            annotated_image,
            results.right_hand_landmarks,
            mp_holistic.HAND_CONNECTIONS, 
            landmark_drawing_spec=mp_drawing_styles
            .get_default_hand_landmarks_style(),
            connection_drawing_spec=mp_drawing_styles
            .get_default_hand_connections_style())

    def __call__(self, img : np.ndarray, annotate=True):

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        results = self._holistic.process(img)

        if annotate:           
            if results.segmentation_mask is None:
                results.segmentation_mask = np.zeros((img.shape[0], img.shape[1]))                 
            annotated_image = img.copy()
            condition = np.stack((results.segmentation_mask,) * 3, axis=-1) > 0.1
            bg_image = np.zeros(img.shape, dtype=np.uint8)
            bg_image[:] = 255
            annotated_image = np.where(condition, annotated_image, bg_image)
            self.draw_results(annotated_image, results)
            return results, cv2.cvtColor(annotated_image, cv2.COLOR_BGR2RGB)
        else:
            return results

class Authenticator():

    FACE_THRESHOLD = 10
    INTRUSION_THRESHOLD = 100
    FACE_TEST_INTERVAL = 5

    def __init__(self):
        valid = False

        self._exp_arm_movement = None
        self._face_encoding = None
        try:
            self._exp_arm_movement = pose.ArmMovement(ARM_CONFIG_PATH)
            self._arm_movement = pose.ArmMovement()
            valid = True
        except:
            pass
        #Old (non working) face recognition method
        #try:            
            #self._face = face.FaceDetection(FACE_CONFIG_PATH)
            #valid = True
        #except:
        #    pass
        try:
            known_image = face_recognition.load_image_file("set/me.jpg")
            self._face_encoding = [face_recognition.face_encodings(known_image)[0]]
            self._face_detect_count = 0
            valid = True
        except:
            pass

        if not valid:
            raise ValueError("Please set up an authentication method")

        self._person_found = False
        self._person_found_count = 0
        self._frame_count = 0

    def __enter__(self):
        self._process_image = ProcessImage()
        self._process_image.__enter__()
        return self

    def __exit__(self, *args):
        self._process_image.__exit__()

    def __call__(self, image):
        valid = True
        self._frame_count += 1
        
        if self._exp_arm_movement:            
            results = self._process_image(image, False)
            if results.pose_landmarks:
                self._person_found = True
                arm_state = pose.ArmState(results.pose_landmarks)
                self._arm_movement.update(arm_state)
            else:
                self._arm_movement.reset()
            if self._arm_movement != self._exp_arm_movement:
                valid = False

        # Old method
        #if self._face:
        #    if results.face_landmarks:
        #        if not self._face.isAuthoritative(results.face_landmarks):
        #            valid = False
        #    else:
        #        valid = False

        if self._face_encoding:
            if self._frame_count % Authenticator.FACE_TEST_INTERVAL == 0:
                encodings = face_recognition.face_encodings(image)
                recognized = False
                for encoding in encodings:
                    self._person_found = True
                    if recognized:
                        break
                    for result in face_recognition.compare_faces(self._face_encoding, encoding):
                        if result:
                            recognized = True
                            break
                if recognized:
                    self._face_detect_count += 1
                    if self._face_detect_count < Authenticator.FACE_THRESHOLD:
                        valid = False
                else:
                    self._face_detect_count = 0  
                    valid = False
            elif self._face_detect_count < Authenticator.FACE_THRESHOLD:
                valid = False           

        if self._person_found:
            self._person_found_count += 1

        return valid

    @property
    def faceDetectionCounter(self):
        return self._face_detect_count

    @property
    def intrusionDetected(self):
        return self._person_found and self._person_found_count > Authenticator.INTRUSION_THRESHOLD

    @property
    def personDetected(self):
        return self._person_found
        
def display_img(windowName, img):
    """Returns False if the window has been closed"""
    cv2.imshow(windowName, img)
    cv2.waitKey(1)
    if cv2.getWindowProperty(windowName, cv2.WND_PROP_VISIBLE) < 1:
        return False
    return True

def process_camera(callback, *args, **kwargs):
    with Camera() as camera:        
        while True:
            ret, img = camera.read()
            if not ret:
                break

            if not callback(img, *args, **kwargs):
                break