#!/bin/env python3

if __name__ != "__main__":
    raise ImportError("This module cannot be imported.")

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("action", choices=["delete_face", "delete_arms", "set_face", "set_arms", "set_face_new"])
args = parser.parse_args()

import os
os.environ["TORCH_SHOW_CPP_STACKTRACES"] = "1"

import pose
from util import *
import PIL
import json

def setAuthoritativeFace():
    image_count = [0]
    def callback(img):
        image_count[0] += 1
        if image_count[0] < 100:
            return display_img("Please show your face to the camera", img)
        PIL.Image.fromarray(img).save(FACE_CONFIG_PATH)

    process_camera(callback)

#def setAuthoritativeFace():
#    fd = face.FaceDetection()
#    fd.startUpdateAsset()

    # IMAGES_COUNT = 200

    # counter = [0]
    # progress = tqdm(total=IMAGES_COUNT, desc="Taking pictures of the new authoritative face")

    # def callback(img):
    #     if fd(img):
    #         counter[0] += 1
    #         progress.update()
    #         if counter[0] >= IMAGES_COUNT:
    #             return False

    #     return display_img("Please show your face to the camera", img)

    # process_camera(callback)

    # progress.close()
    # cv2.destroyAllWindows()
    # fd.saveNewAsset(FACE_CONFIG_PATH)
            
def setExpectedArmMovement():
    am = pose.ArmMovement()
    
    def callback(img, pi):
        results, img = pi(img)
        if results.pose_landmarks:
            arm_state = pose.ArmState(results.pose_landmarks)
            am.update(arm_state)
            if am.validated:
                with open(ARM_CONFIG_PATH, "w") as file:                    
                    json.dump(am.dump(), file)
                return False
        else:
            am.reset()
        return display_img("Please move your arms as appropriate", img)

    with ProcessImage() as pi:
        process_camera(callback, pi)

def deleteAuthoritativeFace():
    os.remove(FACE_CONFIG_PATH)

def deleteArmMovement():
    os.remove(ARM_CONFIG_PATH)


if args.action == "delete_face":
    deleteAuthoritativeFace()
elif args.action == "delete_arms":
    deleteArmMovement()
elif args.action == "set_face":
    setAuthoritativeFace()
elif args.action == "set_arms":
    setExpectedArmMovement()

cv2.destroyAllWindows()
