import math

class Point():

    def __init__(self, data, **kwargs):
        if isinstance(data, tuple):
            self._x = data[0]
            self._y = data[1]
            self._z = data[2]
        else:
            self._x = data.x
            self._y = data.y
            self._z = data.z
            self._z = 0

            if "min_visibility" in kwargs and hasattr(data, "visibility"):
                if data.visibility < kwargs["min_visibility"]:
                    raise ValueError()

    def __repr__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ", " + str(self.z) + ")"

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def z(self):
        return self._z

    def __sub__(self, other):
        return Vector((self.x - other.x, self.y - other.y, self.z - other.z))

class Vector(Point):

    @staticmethod
    def horizontal():
        return Vector((1,0,0))

    @staticmethod
    def vertical():
        return Vector((0,1,0))

    def __add__(self, other):
        return Vector((self.x + other.x, self.y + other.y, self.z + other.z))

    def __mul__(self, other):
        return self.x * other.x + self.y * other.y + self.z * other.z

    def __xor__(self, other): # cross product
        return Vector((self.y * other.z - self.z * other.y,
                        self.z * other.x - self.x * other.z,
                        self.x * other.y - self.y * other.x))
        
    def __neg__(self):
        return Vector((-self.x, -self.y, -self.z))

    def __pos__(self):
        return Vector(self)

    def __iadd__(self, other):
        self._x += other.x
        self._y += other.y
        self._z += other.z
        return self

    def __isub__(self, other):
        self._x -= other.x
        self._y -= other.y
        self._z -= other.z
        return self

    def __ixor__(self, other): # in place cross product        
        result = self ^ other
        self._x = result.x
        self._y = result.y
        self._z = result.z
        return self

    def norm(self):
        return math.sqrt((self.x) ** 2 + (self.y) ** 2 + (self.z) ** 2)

    def cos(self, other):
        dot = self * other
        return dot / (self.norm() * other.norm())

    def sin(self, other):
        cross = self ^ other
        return cross.norm() / (self.norm() * other.norm())
    
    def angle(self, other):
        cos = self.cos(other)
        sin = self.sin(other)
        return math.atan2(sin, cos)