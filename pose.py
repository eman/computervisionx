import mediapipe as mp
mp_pose = mp.solutions.pose
import math
import json

from point import Point, Vector

class ArmState():

    THRESHOLD_DETECTION = 0.0
    THRESHOLD_VISIBILITY = 0.5
    THRESHOLD_DOWN = math.pi * 0.15
    THRESHOLD_STRAIGHT = math.pi * 0.25
    THRESHOLD_BENT = math.pi * 0.7
    THRESHOLD_HORIZONTAL = math.pi * 0.1
    THRESHOLD_UP = math.pi * 0.2

    ARM_POS_UNRECOGNIZED = -1
    ARM_DOWN = 0
    ARM_UP = 1
    ARM_HORIZONTAL = 2
    ARM_DOWN_BENT = 3
    ARM_HORIZONTAL_BENT = 4
    ARM_UP_BENT = 5

    def pos_name(self, pos):
        if pos == ArmState.ARM_DOWN:
            return "DOWN"
        if pos == ArmState.ARM_UP:
            return "UP"
        if pos == ArmState.ARM_HORIZONTAL:
            return "HORIZONTAL"
        if pos == ArmState.ARM_DOWN_BENT:
            return "DOWN BENT"
        if pos == ArmState.ARM_HORIZONTAL_BENT:
            return "HORIZONTAL BENT"
        if pos == ArmState.ARM_UP_BENT:
            return "UP BENT"        
        
        return "UNRECOGNIZED"
    
    def __init__(self, pose_landmark):

        if isinstance(pose_landmark, tuple):
            self.left_arm = pose_landmark[0]
            self.right_arm = pose_landmark[1]
            return

        keys = mp_pose.PoseLandmark
        landmark = pose_landmark.landmark

        try:
            left_shoulder = Point(landmark[keys.LEFT_SHOULDER], min_visibility=ArmState.THRESHOLD_VISIBILITY)
            left_elbow = Point(landmark[keys.LEFT_ELBOW], min_visibility=ArmState.THRESHOLD_VISIBILITY)
            left_wrist = Point(landmark[keys.LEFT_WRIST], min_visibility=ArmState.THRESHOLD_VISIBILITY)

            self.left_arm = self.get_arm_pos(left_shoulder, left_elbow, left_wrist)
        except: # visibility of points below threshold
            self.left_arm = ArmState.ARM_POS_UNRECOGNIZED

        try:
            right_shoulder = Point(landmark[keys.RIGHT_SHOULDER], min_visibility=ArmState.THRESHOLD_VISIBILITY)
            right_elbow = Point(landmark[keys.RIGHT_ELBOW], min_visibility=ArmState.THRESHOLD_VISIBILITY)
            right_wrist = Point(landmark[keys.RIGHT_WRIST], min_visibility=ArmState.THRESHOLD_VISIBILITY)
            
            self.right_arm = self.get_arm_pos(right_shoulder, right_elbow, right_wrist)
        except:
            self.right_arm = ArmState.ARM_POS_UNRECOGNIZED

    def get_arm_pos(self, shoulder, elbow, wrist):
        arm = elbow - shoulder
        if arm.norm() < ArmState.THRESHOLD_DETECTION:
            return ArmState.ARM_POS_UNRECOGNIZED
        angle1 = Vector.vertical().angle(arm)

        forearm = wrist - elbow
        if forearm.norm() < ArmState.THRESHOLD_DETECTION:
            return ArmState.ARM_POS_UNRECOGNIZED
        angle2 = arm.angle(forearm)

        arm_down = angle1 < ArmState.THRESHOLD_DOWN
        arm_horizontal = abs(angle1 - math.pi / 2) < ArmState.THRESHOLD_HORIZONTAL
        arm_up = math.pi - angle1 < ArmState.THRESHOLD_UP

        forearm_straight = angle2 < ArmState.THRESHOLD_STRAIGHT
        forearm_bent = angle2 > ArmState.THRESHOLD_STRAIGHT

        if len(list(filter(lambda x:x, (arm_down, arm_horizontal, arm_up)))) != 1:
            return ArmState.ARM_POS_UNRECOGNIZED
        if len(list(filter(lambda x:x, (forearm_bent, forearm_straight)))) != 1:
            return ArmState.ARM_POS_UNRECOGNIZED

        if arm_down and forearm_straight:
            return ArmState.ARM_DOWN
        if arm_down and forearm_bent:
            return ArmState.ARM_DOWN_BENT
        if arm_horizontal and forearm_straight:
            return ArmState.ARM_HORIZONTAL
        if arm_horizontal and forearm_bent:
            return ArmState.ARM_HORIZONTAL_BENT
        if arm_up and forearm_straight:
            return ArmState.ARM_UP
        if arm_up and forearm_bent:
            return ArmState.ARM_UP_BENT
        return ArmState.ARM_POS_UNRECOGNIZED

    def recognized(self):
        return self.left_arm != ArmState.ARM_POS_UNRECOGNIZED and self.right_arm != ArmState.ARM_POS_UNRECOGNIZED

    def __eq__(self, other):
        if other is None: return False
        return (other.right_arm == ArmState.ARM_POS_UNRECOGNIZED or self.right_arm == other.right_arm) and \
                (other.left_arm == ArmState.ARM_POS_UNRECOGNIZED or self.left_arm == other.left_arm)

    def __repr__(self):
        return "[Left: " + self.pos_name(self.left_arm) + "; right: " + self.pos_name(self.right_arm) + "]"

class ArmMovement:
    
    HOLD_TIME = 5
    VALIDATE_TIME = 20

    def __init__(self, moves=None):
        self._moves = []
        self._acc = 0
        self._current_detection = None
        self._validated = False
        self._frozen = False
        if moves is not None:
            self._load(moves)

    def _load(self, path):
        with open(path) as file:
            data = json.load(file)
        if not isinstance(data, list):
            raise ValueError()
        for move in data:
            if isinstance(move, list) and len(move) == 2:
                a = ArmState((move[0], move[1]))
                self._moves.append(a)
            else:
                self._moves.clear()
                raise ValueError()

        self._frozen = True
        self._validated = True
    
    def update(self, arm_state):
        if self._frozen:
            raise ValueError("Cannot update this instance")

        if self._current_detection == arm_state and arm_state.recognized():
            self._acc += 1
        else:
            self._acc = 0
            self._validated = False

        self._current_detection = arm_state
        if self._acc >= ArmMovement.HOLD_TIME and arm_state.recognized() and \
             (self._moves == [] or self._moves[-1] != arm_state):
            print("Detected: ", arm_state)
            self._moves.append(arm_state)

        if self._acc >= ArmMovement.VALIDATE_TIME:
            self._validated = True

    def reset(self):
        if self._frozen:
            raise ValueError("Cannot reset this instance")

        self._acc = 0
        self._validated = False
        self._current_detection = None
        if len(self._moves):
            print("Detection reset")
        self._moves.clear()
        
    @property
    def validated(self):
        return self._validated

    def __eq__(self, other):
        if other is None:
            return False
        if not self._validated or not other._validated:
            return False
        if len(self._moves) < len(other._moves):
            return False
        return self._moves[-len(other._moves):] == other._moves

    def dump(self):
        lst = []
        for move in self._moves:
            lst.append([move.left_arm, move.right_arm])

        return lst