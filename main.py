#!/bin/env python3

from util import Authenticator, display_img, process_camera, np
import cv2
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--debug", action="store_true")
args = parser.parse_args()

if __name__ == "__main__":
   
    def detect_from_camera(windowName):

        frame_count = [0]

        def callback(img, authenticator):   

            frame_count[0] += 1 

            if not authenticator.intrusionDetected and authenticator(img):
                print("Authentication successful. Exiting")
                return False
        
            if authenticator.personDetected:
                color = [0, 255 * (authenticator.faceDetectionCounter / Authenticator.FACE_THRESHOLD),
                            int(255 * (1 - authenticator.faceDetectionCounter / Authenticator.FACE_THRESHOLD))]
                
                if authenticator.intrusionDetected and frame_count[0] % 10 < 5:
                    img = np.array([0,0,255]) * np.ones(img.shape)
                else:            
                    top, bottom, left, right = [10]*4
                    img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
                
            r = display_img(windowName, img)            
            # Prevent from closing the window except in debug mode
            if args.debug:
                return r

            return True               
        
        with Authenticator() as authenticator:
            process_camera(callback, authenticator)
        cv2.destroyAllWindows()

    detect_from_camera("Stream")
