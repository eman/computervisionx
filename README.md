## Python's Dependencies: ##

* OpenCV2
* MediaPipe
* face_recognition
* argparse
* json
* PyTorch / torchvision (to test old method)
* tqdm (to test old method)
* glob (to test old method)

## Run the code ##

### Setup ###

`python setup.py set_face`

Starts the webcam, takes and saves a picture after a few seconds.
This picture will then be used as the reference for facial recognition.

`python setup.py set_arms`

Starts the webcam and starts registering the move of your arms.
The move is saved when you hold the last pose long enough.

`python setup.py delete_face`

Deletes the picture of your face and disables face recognition.

`python setup.py delete_arms`

Disables arm move recognition.

### Main program ###

You must have set up at least one authentication method to run the program.

Command:
`python main.py`

If you want to be able to close the window without authenticating, run with:
`python main.py --debug`
