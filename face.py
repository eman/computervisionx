import torch, torchvision
import torch.nn as nn
import torchvision.models as models
import glob
from tqdm import tqdm
import cv2
from util import mp_face_mesh, mp_holistic
import random

LANDMARK_SHAPE = (3, 26, 18)
MAX_BATCH_SIZE = 100

class DetectionModel(nn.Module):

    def __init__(self):      
        super().__init__()        
       # self._preprocess = torchvision.transforms.Resize((3, 26, 26))
        self._transform = models.ResNet34_Weights.DEFAULT.transforms()
        self._main_submodel = models.resnet34(num_classes=1)
        self._sigmoid = nn.Sigmoid()

    def _preprocess(self, img):
        assert img.dim() == len(LANDMARK_SHAPE) + 1 and tuple(img.size()[1:]) == LANDMARK_SHAPE
        return self._transform(img)

    def forward(self, img):
        img = self._preprocess(img)
        img = self._main_submodel(img)
        img = self._sigmoid(img)
        return img

class FaceDetection():

    def __init__(self, weights=None):
        self._mp_face = mp_holistic.Holistic(static_image_mode=True)
        self._model = self._init_model(weights)
        
    def _init_model(self, weights=None):
        model = DetectionModel()
        if weights is not None:
            weights = torch.load(weights)
            model.load_state_dict(weights)        
        model.eval()
        return model
    
    def _landmark_to_tensor(self, landmark):
        img = torch.tensor([[l.x, l.y, l.z] for l in landmark.landmark], dtype=torch.float)

        min_x = torch.min(img[:,0])
        min_y = torch.min(img[:,1])
        min_z = torch.min(img[:,2])

        img = img - torch.tensor([min_x, min_y, min_z])

        max_x = torch.max(img[:,0])
        max_y = torch.max(img[:,1])
        max_z = torch.max(img[:,2])

        img = img / max(max_x, max_y, max_z)
        img = img.T

        img.resize_(*LANDMARK_SHAPE)
        return img

    def _process_image(self, landmark):
        if landmark.dim() == 4 and landmark.size(0) > MAX_BATCH_SIZE:
            results = []
            for i in range(landmark.size(0) // MAX_BATCH_SIZE):
                results.append(self._process_image(landmark[MAX_BATCH_SIZE * i: MAX_BATCH_SIZE * (i+1)]))
            results.append(self._process_image(landmark[landmark.size(0) // MAX_BATCH_SIZE * MAX_BATCH_SIZE:]))
            return torch.vstack(results)
            
        return self._model(landmark)

    def __call__(self, landmark):        
        if self.training:
            results = self._mp_face.process(landmark)
            if results.face_landmarks:
                img = self._landmark_to_tensor(results.face_landmarks)
                self._images.append(img)
                return True
            else:
                return False
        else:
            img = self._landmark_to_tensor(landmark)
            if img.dim() == len(LANDMARK_SHAPE):
                img = img.unsqueeze(0)
            img = self._normalize_dataset(img)
            return self._process_image(img)

    def isAuthoritative(self, landmark):
        assert not self.training
        result = self(landmark).item()
        return self._get_prediction(result)

    def _get_prediction(self, result):
        return result > 0.5

    def _path_to_tensor(self, path):
        img = cv2.imread(path)
        result = self._mp_face.process(img)
        
        if result.face_landmarks:
            return self._landmark_to_tensor(result.face_landmarks)
        else:
            return None

    def startUpdateAsset(self):
        self._model = self._init_model()
        self._model.train()
        self._images = []

    def saveNewAsset(self, path, steps=500, batch_size=10):
        self._train(steps, batch_size)
        self._model.eval()
        torch.save(self._model.state_dict(), path)
        self._diagnosis(path)

    def _diagnosis(self, path):
        SAMPLES_PER_STEP = 10
        REPEAT_COUNT = 10

        print("Diagnosis - This step will print the number of false positive results " +
        "in batches of all-negative samples.")

        self.inspectFalsePositive(SAMPLES_PER_STEP, REPEAT_COUNT)
            
        print("Reloading the model to check that its state is persistent:")
        self._model = self._init_model(path)
        self.inspectFalsePositive(SAMPLES_PER_STEP, REPEAT_COUNT)

    def _create_labels(self, neg_count, pos_count):
        neg_labels = torch.zeros((neg_count, 1))
        pos_labels = torch.ones((pos_count, 1))
        return torch.vstack((neg_labels, pos_labels))

    def _split_train_test(self, samples, train_test_ratio):
        test_count = len(samples) // (train_test_ratio + 1)
        return samples[:-test_count], samples[-test_count:]

    def _create_negative_samples(self, count, train_test_ratio):
        paths = glob.glob("train/face/*.jpg")
        if count > len(paths):
            raise ValueError("Not enough negative samples (requested %d, but only %d are available)" % (count, len(paths)))

        random.shuffle(paths)

        progress = tqdm(total=count, desc="Loading negative samples")

        negatives = []
        i = 0
        while len(negatives) < count:
            tensor = self._path_to_tensor(paths[i])
            if tensor is not None:
                negatives.append(tensor)
                progress.update()
            i += 1

        progress.close()

        return self._split_train_test(negatives, train_test_ratio)

    def _normalize_dataset(self, dataset):
        mean = dataset.mean((0,2,3))
        std = dataset.std((0,2,3))
        transform = torchvision.transforms.Normalize(mean, std)
        return transform(dataset)

    def _stack_dataset(self, negative, positive):
        dataset = negative + positive
        dataset = torch.stack(dataset)
        dataset = self._normalize_dataset(dataset)
        return dataset

    def _create_dataset(self, negative, positive):
        dataset = self._stack_dataset(negative, positive)
        labels = self._create_labels(len(negative), len(positive))
        perm = torch.randperm(dataset.size(0))
        return dataset[perm].cpu(), labels[perm].cpu()

    def _create_train_test_set(self, train_test_ratio, neg_pos_ratio):
        number_of_negatives = int(neg_pos_ratio * len(self._images))
        train_negative, test_negative = self._create_negative_samples(number_of_negatives, train_test_ratio)
        
        random.shuffle(self._images)
        train_positive, test_positive = self._split_train_test(self._images, train_test_ratio)
        train_set, train_labels = self._create_dataset(train_negative, train_positive)
        test_set, test_labels = self._create_dataset(test_negative, test_positive)
    
        return train_set, train_labels, test_set, test_labels

    def _create_batch(self, train_set, train_labels, batch_size):
        n = torch.randint(0, train_set.size(0), (batch_size,))
        return train_set[n], train_labels[n]

    def _get_loss_function(self, labels):
        return torch.nn.BCELoss(weight=torch.ones((labels.size(0), 1)) - 0.7 * labels)

    def _train(self, steps, batch_size):
        TEST_INTERVAL = 100
        TRAIN_TEST_RATIO = 3
        NEG_POS_RATIO = 15        

        train_set, train_labels, test_set, test_labels = self._create_train_test_set(train_test_ratio=TRAIN_TEST_RATIO, neg_pos_ratio=NEG_POS_RATIO)
        self._images.clear() #free memory

        optimizer = torch.optim.AdamW(self._model.parameters())

        for i in tqdm(range(steps), desc="Training to recognize the new face..."):
            batch_set, batch_labels = self._create_batch(train_set, train_labels, batch_size)
            
            optimizer.zero_grad()
            outputs = self._process_image(batch_set)
            loss = self._get_loss_function(batch_labels)
            l = loss(outputs, batch_labels)    
            l.backward()  
            optimizer.step()

            if i % TEST_INTERVAL == 0 or i == steps - 1:
                with torch.no_grad():
                    print("")
                    print("Epoch", i, ". Computing test loss...", flush=True)
                    self._model.eval()
                    self._validation_step_verbose(test_set, test_labels)
                    self.inspectFalsePositive(20)
                    print("")
                    self._model.train()

    def _validation_step_verbose(self, test_set, test_labels):
        loss = self._get_loss_function(test_labels)
        results, test_predictions = self._get_validation_results(test_set)
        test_loss = (loss(results, test_labels).sum() / test_labels.size(0)).item()
        expected_predictions = self._get_prediction(test_labels)

        test_recall = (test_predictions & expected_predictions).sum() / expected_predictions.sum()
        test_acc = (test_predictions & expected_predictions).sum() / test_predictions.sum()
        
        print("Current (normalized) test loss: ", test_loss)
        print("Current test recall: ", test_recall) 
        print("Current test accuracy: ", test_acc)            
        print("Number of false-positive: %d/%d " % ((test_predictions & torch.logical_not(expected_predictions)).sum(), torch.logical_not(expected_predictions).sum()))
        print("Number of false-negative: %d/%d " % ((torch.logical_not(test_predictions) & expected_predictions).sum(), expected_predictions.sum()))
        print("Proportion of well-predicted samples: ", (test_predictions == expected_predictions).mean(dtype=torch.float))

    def _get_validation_results(self, test_set):
        results = self._process_image(test_set)       
        test_predictions = self._get_prediction(results)       
        return results, test_predictions

    def inspectFalsePositive(self, number_of_samples, repeat=1):
        for _ in range(repeat):
            _, test_set = self._create_negative_samples(number_of_samples, 0)
            test_set = torch.stack(test_set)
            test_set = self._normalize_dataset(test_set)
            _, predictions = self._get_validation_results(test_set)
            print("False positive results: ", predictions.sum())

    @property
    def training(self):
        return self._model.training
